import re

from bs4 import *

class baseTag:
	def __init__(self,tag):
		self.tag=tag
	
	def Parse(self,data):
		bs=BeautifulSoup(data)
		tag=self.tag
		allNode=bs.findAll(tag)
		self.__contentBTWNode=[]#content between nodes
		for i in allNode:
			uniqueContent=self.__ConcatenateContent(i.contents)
			attriKey=i.attrs
			attri={}
			for a in attriKey:
				attri[a]=i[a]
			self.__contentBTWNode.append((uniqueContent,attri))
	def GetAllContent(self):
		contentList=[]
		for c in self.__contentBTWNode:
			contentList.append(c[0])
		return contentList
	def GetAllContentWithAttribute(self):
		return self.__contentBTWNode
	def __ConcatenateContent(self,contentList):#convert to str and linking all list to a string
		_contentList=[]
		for c in contentList:
			strC=str(c)
			_contentList.append(strC.strip())
		return ''.join(_contentList)
		