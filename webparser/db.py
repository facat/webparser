
import mongoengine


class Configure(mongoengine.Document):
	name=mongoengine.StringField(required=True,unique=True)
	bitStream=mongoengine.BinaryField()
	pass
class Page(mongoengine.Document):
	_url=mongoengine.StringField(required=True,unique=True)
	_pageContent=mongoengine.StringField()

class DabaBaseManager:
	def SetConfigure(self,configName,bit):
		self.__connect()
		search=Configure.objects(name=configName)
		if len(search)==0:#create one
			collection=Configure()
		else:
			collection=search[0]
		collection.name=configName
		collection.bitStream=bit
		collection.save()					
	def GetConfigure(self,configName):
		self.__connect()
		search=Configure.objects(name=configName)		
		if len(search)==0:
			return None
		bitStream=search[0].bitStream 
		return bitStream
	def ChechUrlExist(self,url):
		url=url.strip().lower()
		self.__connect()
		search=Page.objects(_url=url)
		if len(search)==0:
			return False
		else:
			return True
	def PutPage(self,url,page):
		url=url.strip().lower()
		self.__connect()
		search=Page.objects(_url=url)
		if len(search)==0:
			Page(_url=url,_pageContent=page).save()
		else:
			collection=search[0]
			collection._pageContent=page
			collection.save()
	def __connect(self):
		mongoengine.connect('machinelearning')
		

if __name__=='__main__':
	mongoengine.connect('machinelearning')
	a=Configure()
	a.name='fsd'
	a.bitStream=b'fsd'
	a.save()
	print('Finished')
	pass