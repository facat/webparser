from webparser.base import *

class liTag(baseTag):
	def __init__(self):
		super(liTag,self).__init__('li')

class aTag(baseTag):
	def __init__(self):
		super(aTag,self).__init__('a')	
	pass