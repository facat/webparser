import time
import hashlib
import random

def __GetTime():
	return time.time()
	pass

def GetUniqueID():
	time=str(__GetTime()+random.random()).encode()
	m=hashlib.md5()
	m.update(time)
	return m.hexdigest().lower()
	pass
	