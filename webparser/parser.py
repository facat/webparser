import urllib.request
from webparser.tag import *
import socket
import builtins
import pickle
import os
import mongoengine
from webparser.db import *

import threading


class ParseThread(threading.Thread):
	def __init__(self,url,finisheFun,webParserIns):
		self.__finishedFun=finisheFun
		#self.__finishedCallable=finishedCallable
		self.__urlPage=None
		self.__url=url
		self.__webParserIns=webParserIns
	def run(self):
		url=self.__url
		a=WebReader()
		page=None
		try:
			page=a.read(url)
			if page is None:
				return None			
			page.decode('utf-8')
		except builtins.UnicodeDecodeError:
			return
		except builtins.UnicodeEncodeError:
			return
		self.__urlPage=(url,page)	
	def GetUrlPage(self):
		return self.__urlPage#return a tuple
	#def __AquireUrl(self):
		#fun=self.__aquireUrlCallable
		#return fun()
	def __FinishedCallBack(self):
		finishedFun=self.__finishedFun
		finishedFun(self.__webParserIns,self)
		


class WebParser:
	def __init__(self,startUrl,maxVisit=10):
		self.__startUrl=startUrl
		self.__maxVisit=maxVisit
		self.visitCount=0
		self.threadLock=threading.Lock()
	def Start(self):
		startUrl=self.__startUrl
		page=self.__ReadPage(startUrl)
		fatherPageListPara=self.__Deserialization('fatherPageListPara')
		if fatherPageListPara is None:
			fatherPageListPara=[(startUrl,page)]
		fatherItePara=self.__Deserialization('fatherItePara')
		if fatherItePara is None:
			fatherItePara=0
		childPageListPara=self.__Deserialization('childPageListPara')
		childItePara=self.__Deserialization('childItePara')
		nextChildPagesDicPara=self.__Deserialization('nextChildPagesDicPara')
		nextChildPagesListPara=self.__Deserialization('nextChildPagesListPara')
		if self.__VisitPages(fatherPageListPara,fatherItePara,childPageListPara,childItePara,nextChildPagesDicPara,nextChildPagesListPara) is False:
			self.__Serialization(self.__fatherPageList,'fatherPageListPara')
			self.__Serialization(self.__iteInFatherPageList,'fatherItePara')
			self.__Serialization(self.__currentChildPageList,'childPageListPara')
			self.__Serialization(self.__iteInCurrentChildPageList,'childItePara')
			self.__Serialization(self.__nextChildPageDic,'nextChildPagesDicPara')
			self.__Serialization(self.__nextChildPageList,'nextChildPagesListPara')
	def PageHandle(self,page,url=None):
		return
	def SavePageContent(self,url,pageContent):
		self.__SavePageContent(url, pageContent)
	def __ReadPage(self,url):
		a=WebReader()
		page=None
		try:
			page=a.read(url)
			if page is None:
				return None			
			page.decode('utf-8')
		except builtins.UnicodeDecodeError:
			return	None
		except builtins.UnicodeEncodeError:
			return None
		return page
	def __VisitPages(self,fatherPageListPara,fatherItePara=0,hrefPara=None,childItePara=None,nextChildPagesDicPara=None,nextChildPagesListPara=None):
			#_pages=list(fatherPageListPara)
			fatherIte=fatherItePara
			fatherPageList=fatherPageListPara
			while len(fatherPageList)!=0:
				if nextChildPagesDicPara is None:
					childPagesDic={}
				else:
					childPagesDic=nextChildPagesDicPara
					nextChildPagesDic=None
				if nextChildPagesListPara is None:
					childPagesList=[]
				else:
					childPagesList=nextChildPagesListPara
					nextChildPagesListPara=None
				while fatherIte<len(fatherPageList):			
					atag=aTag()
					p=fatherPageList[fatherIte]#p is string text
					atag.Parse(p[1])#p is a tuple e.g (url, contnet). url and  content are strings.
					
					if childItePara is None:
						childIte=0
					else:
						childIte=childItePara
						childItePara=None
					if hrefPara is None:
						href=atag.GetAllContentWithAttribute()
					else:
						href=hrefPara
						hrefPara=None
					while childIte<len(href):
						txt,attrib=href[childIte]
						if 'href' in attrib.keys():
							url=attrib['href']
						else:
							childIte+=1
							continue
						if url in childPagesDic:
							childIte+=1
							continue
						if self.__CheckUrlInDB(url):
							childIte+=1
							continue
						if not url.startswith('http'):
							childIte+=1
							continue
						pageContent=self.__ReadPage(url)
						if pageContent is None:
							childIte+=1
							continue
						print(attrib['href'])
						childPagesDic[url]=''#childPagesDic record if a replicated is to be added
						childPagesList.append((url,pageContent))
						#save page content in DB
						self.__SavePageContent(url, pageContent)
						visitCount=self.visitCount
						if visitCount>=self.__maxVisit:
							self.__iteInFatherPageList=fatherIte
							self.__fatherPageList=fatherPageList
							self.__iteInCurrentChildPageList=childIte
							self.__currentChildPageList=href
							self.__nextChildPageList=childPagesList
							self.__nextChildPageDic=childPagesDic
							return False#False means reach the max visitCount
						visitCount+=1
						self.__visitCount=visitCount
						print('current visit: %d'%(visitCount))	
						childIte+=1
					fatherIte+=1
				#Do something
				for p in fatherPageList:
					self.PageHandle(p[0],p[1])				
				fatherPageList=childPagesList
				print('current child pages length%d'%(len(childPagesList)))	
	def __VisitChildPages(self,childIte,href,childPagesDic,childPagesList):
		emitThread=1
		self.threadChildIte=childIte
		self.threadHref=href
		self.threadChildPagesDic=childPagesDic
		self.threadchildPagesList=childPagesList
		for e in range(emitThread):
			if e>len(href)-1:
				break
			newThread=ParseThread(self.FinishedCallBack)
			newThread.start()
		pass
	@staticmethod
	def FinishedCallBack(webParseIns,threadIns):
		webParseIns.hreadLock.acquire()
		childIte=webParseIns.threadChildIte
		href=webParseIns.threadHref
		threadLock=webParseIns.threadLock
		#childPagesDic=webParseIns.threadChildPagesDic
		childPagesList=webParseIns.threadchildPagesList
		if childIte>=len(href):
			threadLock.release()	
			return
		childIte+=1
		pageContent=threadIns.GetUrlPage()
		childPagesList.append(pageContent)
		webParseIns.SavePageContent(url, pageContent)
		visitCount=webParseIns.visitCount
		if visitCount>=self.__maxVisit:
			self.__iteInFatherPageList=fatherIte
			self.__fatherPageList=fatherPageList
			self.__iteInCurrentChildPageList=childIte
			self.__currentChildPageList=href
			self.__nextChildPageList=childPagesList
			self.__nextChildPageDic=childPagesDic
			return False#False means reach the max visitCount
		visitCount+=1
		self.__visitCount=visitCount
		print('current visit: %d'%(visitCount))			
		url=href[childIte][0]
		newThread=ParseThread(url, WebParser.FinishedCallBack, webParseIns)
		threadLock.release()
		newThread.start()
	def __Serialization(self,obj,objName,host='localhost'):
		if objName is None:
			raise Exception("objName cannot be None!")
		bit=pickle.dumps(obj)
		m=DabaBaseManager()
		m.SetConfigure(objName,bit)	
	def __Deserialization(self,objName,host='localhost'):
		m=DabaBaseManager()
		bitStream=m.GetConfigure(objName)
		obj=pickle.loads(bitStream)
		return obj
	def __CheckUrlInDB(self,url):
		m=DabaBaseManager()
		return m.ChechUrlExist(url)
	def __SavePageContent(self,url,page):
		m=DabaBaseManager()
		m.PutPage(url, page)
	pass

class WebReader:
	def read(self,url):
		req = urllib.request.Request(url, headers={'User-Agent' : "Magic Browser"})
		webpage=None 
		content=None
		try:
			webpage=urllib.request.urlopen(req,timeout=20)
			header=webpage.getheader('Content-type')
			if not self.__checkAvailableType(header):#only parse html and xml, plain text
				return None
			content=webpage.read()
		except socket.timeout:
			print('timeout')
			return None
		except urllib.error.URLError as e:
			print(e.reason)
			return None
		finally:
			if webpage is not None:
				webpage.close()
		return content
	def __checkAvailableType(self,header):
		availableHeader=('html','xml','plain')
		header=header.strip().lower()
		for a in availableHeader:
			if a in header:
				return True
		return False